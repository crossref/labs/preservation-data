from airflow.decorators import dag, task

from datetime import datetime
from datetime import timedelta

from airflow.decorators import dag, task

API_URL = "https://api.crossref.org"

CODE_BUCKET = "airflow-crossref-research-annotation"
CARINIANA_URL = (
    "http://reports-lockss.ibict.br/keepers/pln/ibictpln/"
    "keepers-IBICTPLN-report.csv"
)
CLOCKSS_URL = "https://reports.clockss.org/keepers/keepers-CLOCKSS-report.csv"
LOCKSS_URL = "https://reports.lockss.org/keepers/keepers-LOCKSS-report.csv"
PKP_URL = "https://pkp.sfu.ca/files/pkppn/onix.csv"

# NOTE: PORTICO_URL is defined in the task below as passing a .txt file in
# an argument seems to trigger a Jina templating bug in Airflow 2.0.0!

# NOTE: HathiTrust variables are defined in the task below as passing a .txt
# file in an argument seems to trigger a Jina templating bug in Airflow 2.0.0!

# NOTE: OCUL variables are defined in the task below as passing a .txt
# file in an argument seems to trigger a Jina templating bug in Airflow 2.0.0!

# NOTE: Export report variables are defined in the task below.

DEFAULT_ARGS = {
    "owner": "meve",
    "depends_on_past": False,
    "email": "labs@crossref.org",
    "email_on_failure": False,
    "email_on_retry": False,
}

DATABASE_VERSION = "0.0.44"


@dag(
    default_args=DEFAULT_ARGS,
    schedule_interval="@monthly",
    catchup=False,
    dagrun_timeout=timedelta(hours=16),
    start_date=datetime(2023, 1, 29),
    tags=["sampling", "preservation"],
)
def import_preservations():
    @task.virtualenv(
        task_id="import_clockss",
        requirements=[
            "requests",
            "boto3",
            f"preservation-database=={DATABASE_VERSION}",
            "psycopg2-binary",
            "crossrefapi",
            "django",
            "pytz",
        ],
        system_site_packages=True,
    )
    def import_clockss(code_bucket, url):
        import preservationdatabase.environment as environment

        environment.setup_environment(code_bucket)

        import logging
        from preservationdatabase.models import ClockssPreservation

        from preservationdatabase.models import LastFill

        if LastFill.will_cache(ClockssPreservation.name()):
            logging.info(
                f"{ClockssPreservation.name()} "
                f"was already imported within the cache period."
            )
            return

        logging.info(f"Starting {ClockssPreservation.name()} " f"import.")

        ClockssPreservation.import_data(url=url)

        logging.info(f"{ClockssPreservation.name()} import " f"complete.")

    @task.virtualenv(
        task_id="import_pkp",
        requirements=[
            "requests",
            "boto3",
            f"preservation-database=={DATABASE_VERSION}",
            "psycopg2-binary",
            "crossrefapi",
            "django",
            "pytz",
        ],
        system_site_packages=True,
    )
    def import_pkp(code_bucket, url):
        import preservationdatabase.environment as environment

        environment.setup_environment(code_bucket)

        import logging
        from preservationdatabase.models import PKPPreservation

        from preservationdatabase.models import LastFill

        if LastFill.will_cache(PKPPreservation.name()):
            logging.info(
                f"{PKPPreservation.name()} "
                f"was already imported within the cache period."
            )
            return

        logging.info(f"Starting {PKPPreservation.name()} " f"import.")

        PKPPreservation.import_data(url=url)

        logging.info(f"{PKPPreservation.name()} import " f"complete.")

    @task.virtualenv(
        task_id="import_lockss",
        requirements=[
            "requests",
            "boto3",
            f"preservation-database=={DATABASE_VERSION}",
            "psycopg2-binary",
            "crossrefapi",
            "django",
            "pytz",
        ],
        system_site_packages=True,
    )
    def import_lockss(code_bucket, url):
        import preservationdatabase.environment as environment

        environment.setup_environment(code_bucket)

        import logging
        from preservationdatabase.models import LockssPreservation

        from preservationdatabase.models import LastFill

        if LastFill.will_cache(LockssPreservation.name()):
            logging.info(
                f"{LockssPreservation.name()} "
                f"was already imported within the cache period."
            )
            return

        logging.info(f"Starting {LockssPreservation.name()} " f"import.")

        LockssPreservation.import_data(url=url)

        logging.info(f"{LockssPreservation.name()} import " f"complete.")

    @task.virtualenv(
        task_id="import_cariniana",
        requirements=[
            "requests",
            "boto3",
            f"preservation-database=={DATABASE_VERSION}",
            "psycopg2-binary",
            "crossrefapi",
            "django",
            "pytz",
        ],
        system_site_packages=True,
    )
    def import_cariniana(code_bucket, url):
        import preservationdatabase.environment as environment

        environment.setup_environment(code_bucket)

        import logging
        from preservationdatabase.models import CarinianaPreservation

        from preservationdatabase.models import LastFill

        if LastFill.will_cache(CarinianaPreservation.name()):
            logging.info(
                f"{CarinianaPreservation.name()} "
                f"was already imported within the cache period."
            )
            return

        logging.info(f"Starting {CarinianaPreservation.name()} " f"import.")

        CarinianaPreservation.import_data(url=url)

        logging.info(f"{CarinianaPreservation.name()} import " f"complete.")

    @task.virtualenv(
        task_id="import_portico",
        requirements=[
            "requests",
            "boto3",
            f"preservation-database=={DATABASE_VERSION}",
            "psycopg2-binary",
            "crossrefapi",
            "django",
            "pytz",
        ],
        system_site_packages=True,
    )
    def import_portico(code_bucket):
        PORTICO_URL = "https://api.portico.org/kbart/Portico_Holding_KBart.txt"
        import preservationdatabase.environment as environment

        environment.setup_environment(code_bucket)

        import logging
        from preservationdatabase.models import PorticoPreservation

        from preservationdatabase.models import LastFill

        if LastFill.will_cache(PorticoPreservation.name()):
            logging.info(
                f"{PorticoPreservation.name()} "
                f"was already imported within the cache period."
            )
            return

        logging.info(f"Starting {PorticoPreservation.name()} " f"import.")

        PorticoPreservation.import_data(url=PORTICO_URL)

        logging.info(f"{PorticoPreservation.name()} import " f"complete.")

    @task.virtualenv(
        task_id="import_internet_archive",
        requirements=[
            "requests",
            "boto3",
            f"preservation-database=={DATABASE_VERSION}",
            "psycopg2-binary",
            "crossrefapi",
            "django",
            "pytz",
        ],
        system_site_packages=True,
    )
    def import_internet_archive(code_bucket):
        IA_BUCKET = "preservation.research.crossref.org"
        IA_KEY = "ia_sim_keepers_kbart.2022-12-12.tsv"

        import preservationdatabase.environment as environment
        import boto3

        environment.setup_environment(code_bucket)

        import logging
        from preservationdatabase.models import InternetArchivePreservation

        from preservationdatabase.models import LastFill

        if LastFill.will_cache(InternetArchivePreservation.name()):
            logging.info(
                f"{InternetArchivePreservation.name()} "
                f"was already imported within the cache period."
            )
            return

        logging.info(
            f"Starting {InternetArchivePreservation.name()} " f"import."
        )

        s3client = boto3.client("s3")

        InternetArchivePreservation.import_data(
            IA_KEY, bucket=IA_BUCKET, s3client=s3client
        )

        logging.info(f"{InternetArchivePreservation.name()} import complete.")

    @task.virtualenv(
        task_id="import_hathi",
        requirements=[
            "requests",
            "boto3",
            f"preservation-database=={DATABASE_VERSION}",
            "psycopg2-binary",
            "crossrefapi",
            "django",
            "pytz",
        ],
        system_site_packages=True,
    )
    def import_hathi(code_bucket):
        HATHI_BUCKET = "preservation.research.crossref.org"
        HATHI_KEY = "hathi_full_20230101.txt"

        import preservationdatabase.environment as environment

        environment.setup_environment(code_bucket)

        import boto3
        import logging
        from preservationdatabase.models import HathiPreservation

        from preservationdatabase.models import LastFill

        if LastFill.will_cache(HathiPreservation.name()):
            logging.info(
                f"{HathiPreservation.name()} "
                f"was already imported within the cache period."
            )
            return

        logging.info(f"Starting {HathiPreservation.name()} " f"import.")

        s3client = boto3.client("s3")

        HathiPreservation.import_data(
            HATHI_KEY, bucket=HATHI_BUCKET, s3client=s3client
        )

        logging.info(f"{HathiPreservation.name()} import " f"complete.")

    @task.virtualenv(
        task_id="import_ocul",
        requirements=[
            "requests",
            "boto3",
            f"preservation-database=={DATABASE_VERSION}",
            "psycopg2-binary",
            "crossrefapi",
            "django",
            "pytz",
        ],
        system_site_packages=True,
    )
    def import_ocul(code_bucket):
        OCUL_FILE = "scholars_portal_keepers_20230202.xml"
        OCUL_BUCKET = "preservation.research.crossref.org"

        import preservationdatabase.environment as environment

        environment.setup_environment(code_bucket)

        import boto3
        import logging
        from preservationdatabase.models import OculScholarsPortalPreservation

        from preservationdatabase.models import LastFill

        if LastFill.will_cache(OculScholarsPortalPreservation.name()):
            logging.info(
                f"{OculScholarsPortalPreservation.name()} "
                f"was already imported within the cache period."
            )
            return

        logging.info(
            f"Starting {OculScholarsPortalPreservation.name()} " f"import."
        )

        s3client = boto3.client("s3")

        OculScholarsPortalPreservation.import_data(
            OCUL_FILE, bucket=OCUL_BUCKET, s3client=s3client
        )
        logging.info(
            f"{OculScholarsPortalPreservation.name()} import " f"complete."
        )

    @task.virtualenv(
        task_id="create_member_reports",
        requirements=[
            "requests",
            "boto3",
            f"preservation-database=={DATABASE_VERSION}",
            "psycopg2-binary",
            "crossrefapi",
            "django",
            "pytz",
            "joblib",
            "rich",
        ],
        system_site_packages=True,
    )
    def create_member_reports(code_bucket):
        """
        Generate preservation data for all samples
        :return: None
        """
        SAMPLES_BUCKET = "samples-crossref-research"
        SAMPLES_PATH = "members-works/sample-2023-01-21/works/"

        ANNOTATION_BUCKET = "outputs.research.crossref.org"
        ANNOTATION_PATH = "annotations"
        ANNOTATION_FILENAME = "preservation.json"

        PARALLEL_JOBS = 5

        # setup environment
        import preservationdatabase.environment as environment

        environment.setup_environment(code_bucket)

        import boto3
        import logging

        s3client = boto3.client("s3")

        from preservationdatabase import exporter
        from joblib import Parallel, delayed

        # get member list from S3
        members = exporter.get_members(
            s3client=s3client,
            samples_bucket=SAMPLES_BUCKET,
            samples_path=SAMPLES_PATH,
        )

        logging.info(f"There are {len(members)} to process.")

        Parallel(n_jobs=PARALLEL_JOBS)(
            delayed(exporter.process_sample)(
                ANNOTATION_BUCKET,
                ANNOTATION_FILENAME,
                ANNOTATION_PATH,
                SAMPLES_BUCKET,
                SAMPLES_PATH,
                member_id,
                code_bucket,
            )
            for member_id in members
        )

        return

    # tasks
    t1 = import_clockss(CODE_BUCKET, CLOCKSS_URL)
    t2 = import_pkp(CODE_BUCKET, PKP_URL)
    t3 = import_lockss(CODE_BUCKET, LOCKSS_URL)
    t4 = import_cariniana(CODE_BUCKET, CARINIANA_URL)
    t5 = import_portico(CODE_BUCKET)
    t6 = import_hathi(CODE_BUCKET)
    t7 = import_ocul(CODE_BUCKET)
    t8 = import_internet_archive(CODE_BUCKET)

    # export / reporting tasks
    t9 = create_member_reports(CODE_BUCKET)

    [t1, t2, t3, t4, t5, t6, t7, t8] >> t9


import_preservations()
