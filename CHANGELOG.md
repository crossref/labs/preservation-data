# Changelog

## 2023-02-09
* Filter samples by journal article. Eventually this should be done by triggering the sampling framework.
* Switch to kebab case for JSON outputs to match Crossref API.
